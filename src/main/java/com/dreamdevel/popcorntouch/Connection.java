package com.dreamdevel.popcorntouch;

import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by George on 25/3/2017.
 */

public class Connection implements Serializable {
    private static final String TAG = "Connection";
    private String mName;
    private String mIP;
    private String mPort;
    private String mUsername;
    private String mPassword;

    public Connection () {

    }
    public Connection (String name, String ip, String port, String username, String password) {
        setName(name);
        setIP(ip);;
        setPort(port);
        setUsername(username);
        setPassword(password);
    }

    public Connection (JSONObject connection) {
        parseJSONConnection(connection);
    }

    public void parseJSONConnection(JSONObject connection) {
        try {
            setName("Name not set");
            setUsername(connection.getString("user"));
            setPassword(connection.getString("pass"));
            setPort(connection.getString("port"));
            setIP(connection.getString("ip"));
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't parse JSON Connection",ex);
        }
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getIP() {
        return mIP;
    }

    public void setIP(String IP) {
        mIP = IP;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        mPort = port;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }
}
