package com.dreamdevel.popcorntouch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dreamdevel.popcornapi.Item;
import com.dreamdevel.popcornapi.ItemCollection;

/**
 * Created by George on 22/3/2017.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {
    private Context context;
    private ItemCollection mItemCollection;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;



    public ItemAdapter(Context context, ItemCollection itemCollection) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        mItemCollection = itemCollection;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mItemCollection.getList().size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = mItemCollection.getList().get(position);

        Glide.with(this.context).load(item.getCover()).into(holder.imageView);
        holder.titleTextView.setText(item.getTitle());
        holder.yearTextView.setText(item.getYear());
        holder.ratingTextView.setText(Double.toString(item.getRating()));
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public TextView titleTextView;
        public TextView yearTextView;
        public TextView ratingTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView
                    .findViewById(R.id.coverImageView);

            titleTextView = (TextView) itemView
                    .findViewById(R.id.titleTextView);

            yearTextView = (TextView) itemView
                    .findViewById(R.id.yearTextView);

            ratingTextView = (TextView) itemView
                    .findViewById(R.id.ratingTextView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    public Item getItem(int id) {
        return mItemCollection.getList().get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
