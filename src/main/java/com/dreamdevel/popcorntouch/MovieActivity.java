package com.dreamdevel.popcorntouch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dreamdevel.popcornapi.Item;
import com.dreamdevel.popcornapi.PopcornAPIResult;
import com.dreamdevel.popcornapi.Remote;
import com.dreamdevel.popcornapi.TorrentInfo;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import org.json.JSONArray;
import org.json.JSONObject;

public class MovieActivity extends AppCompatActivity implements YouTubeThumbnailView.OnInitializedListener, PopcornAPIResult {
    private static final String TAG = "MovieActivity";
    public static final String EXTRA_MOVIE_OBJECT = "com.dreamdevel.popcorntouch.movieObject";
    private static final String YOUTUBE_API_KEY = "AIzaSyCaaSd7hurltZciz7Jrd8kgPIU5fA3hoHI";

    private Item mMovieItem;

    private TextView mTitleTextView;
    private TextView mYearTextView;
    private TextView mRuntimeTextView;
    private TextView mSynopsisTextView;
    private ImageView mCoverImageView;
    private TextView mRatingTextView;
    private ImageButton mPlayTrailerButton;
    private Button mPlayMovieButton;
    private ImageButton mBackButton;
    private TextView mGenreTextView;
    private YouTubeThumbnailView mYouTubeThumbnailView;
    private YouTubeThumbnailLoader mThumbnailLoader;
    private TextView mFileSizeTextView;
    private TextView mSeedsTextView;
    private TextView mPeersTextView;
    private TextView mHealthTextView;

    private Integer mGetSelectionMethodID;
    private Integer mGetViewStackMehodID;
    private Integer mGetSubtitlesMethodID;

    public static Intent newMovieIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext,MovieActivity.class);
        intent.putExtra(EXTRA_MOVIE_OBJECT,item);
        return intent;
    }

    public static Intent newMovieIntent(Context packageContext) {
        Intent intent = new Intent(packageContext,MovieActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        connectViewsWithObjects();
        setupViewListeners();
        handleExtras();
        initialize();
    }

    private void connectViewsWithObjects () {
        mTitleTextView = (TextView) findViewById(R.id.titleTextView);
        mYearTextView = (TextView) findViewById((R.id.yearTextView));
        mSynopsisTextView = (TextView) findViewById(R.id.synopsisTextView) ;
        mRuntimeTextView = (TextView) findViewById(R.id.runtimeTextView);
        mCoverImageView = (ImageView) findViewById(R.id.coverImageView);
        mPlayTrailerButton = (ImageButton) findViewById(R.id.playTrailerButton);
        mRatingTextView = (TextView) findViewById(R.id.ratingTextView);
        mBackButton = (ImageButton) findViewById(R.id.backButton);
        mYouTubeThumbnailView = (YouTubeThumbnailView) findViewById(R.id.youtube_view);
        mGenreTextView = (TextView) findViewById(R.id.genreTextView);
        mFileSizeTextView = (TextView) findViewById(R.id.fileSizeTextView);
        mSeedsTextView = (TextView) findViewById(R.id.seedsTextView);
        mPeersTextView = (TextView) findViewById(R.id.peersTextView);
        mHealthTextView = (TextView) findViewById(R.id.healthTextView);
        mPlayMovieButton = (Button) findViewById(R.id.playMovieButton);
    }

    private void setupViewListeners () {
        mBackButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.d(TAG,"Back Button Pressed");
                onBackPressed();
            }
        });

        mPlayMovieButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new Remote().togglePlaying();
                navigateToControl();
            }
        });

        mPlayTrailerButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new Remote().watchTrailer();
                navigateToControlTrailer();
            }
        });
    }

    private void initialize () {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.parseColor("#50000000"));
            // TODO set color of semi-transparent status bar in colors

            mGetViewStackMehodID = new Remote().getViewStack(this);
        }

        // Ask for subtitles
        mGetSubtitlesMethodID = new Remote().getSubtitles(this);
    }

    private void handleExtras () {
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(EXTRA_MOVIE_OBJECT)) {
            mMovieItem = (Item) getIntent().getExtras().getSerializable(EXTRA_MOVIE_OBJECT);
            updateMovieDetails();
        } else {
            getMovieDetails();
        }
    }


    private void getMovieDetails() {
        mGetSelectionMethodID = new Remote().getSelection(this);
    }

    public void onPopcornAPIResult(JSONObject result, Integer methodID) {

        if (methodID == mGetSelectionMethodID) {
            Log.d(TAG,result.toString());
            //updateMovieDetails();
            try {
                mMovieItem = new Item(result.getJSONObject("result"));
                updateMovieDetails();
            } catch (Exception ex) {
                Log.w(TAG,"Couldn't parse movie item",ex);
            }
        } else if (methodID == mGetViewStackMehodID) {
            Log.d(TAG,result.toString());
            try {
                JSONObject resultOfResult = result.getJSONObject("result");
                JSONArray viewStack = resultOfResult.getJSONArray("viewstack");

                if (viewStack.length() == 2 && viewStack.getString(1).equals("app-overlay")) {
                    // Loading Screen
                    navigateToControl();
                } else if (viewStack.length() > 2 &&  (viewStack.getString(2).equals("player") || viewStack.getString(2).equals("notificationWrapper")) ) {
                    if (viewStack.getString(1).equals("app-overlay")) {
                        navigateToControl();
                    } else if (viewStack.getString(1).equals("movie-detail")) {
                        navigateToControlTrailer();
                    }
                }

            } catch (Exception ex) {
                Log.w(TAG,"Couldn't parse getviewstack result",ex);
            }
        } else if (methodID == mGetSubtitlesMethodID) {
            Log.d(TAG,result.toString());
        }
    }

    private void updateMovieDetails () {
        // TODO change this based on user preferences OR check selection ?
        TorrentInfo torrentInfo = mMovieItem.getTorrents().get("720p");
        mFileSizeTextView.setText(torrentInfo.getFileSize());
        mSeedsTextView.setText(Integer.toString(torrentInfo.getSeed()));
        mPeersTextView.setText(Integer.toString(torrentInfo.getPeer()));
        mHealthTextView.setText(torrentInfo.getHealth().substring(0, 1).toUpperCase() + torrentInfo.getHealth().substring(1));

        mTitleTextView.setText(mMovieItem.getTitle());
        mYearTextView.setText(mMovieItem.getYear());
        mSynopsisTextView.setText(mMovieItem.getSynopsis());
        mRuntimeTextView.setText(mMovieItem.getRuntime());
        Glide.with(this).load(mMovieItem.getCover()).into(mCoverImageView);
        mRatingTextView.setText(Double.toString(mMovieItem.getRating()));

        String genreStr = "";
        for (String genre : mMovieItem.getGenre()){
            genreStr += genre + " | ";
        }
        if (genreStr.length() > 0)
            genreStr = genreStr.substring(0,genreStr.length()-1);
        mGenreTextView.setText(genreStr);

        mYouTubeThumbnailView.initialize(YOUTUBE_API_KEY,MovieActivity.this);
    }

    private void navigateToControl() {
        Intent intent = ControlActivity.newControlIntent(MovieActivity.this,mMovieItem);
        startActivity(intent);
    }

    private void navigateToControlTrailer () {
        Intent intent = ControlActivity.newControlTrailerIntent(MovieActivity.this,mMovieItem);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mThumbnailLoader != null)
            mThumbnailLoader.release();
    }

    public void onBackPressed() {
        new Remote().back();
        finish();
    }

    public void onInitializationSuccess(YouTubeThumbnailView thumbnailView, YouTubeThumbnailLoader thumbnailLoader) {
        mThumbnailLoader = thumbnailLoader;
        String trailerUrl = mMovieItem.getTrailer();
        if(!trailerUrl.equals("false")) {
            mPlayTrailerButton.setVisibility(View.VISIBLE);
            Log.d(TAG,trailerUrl);
            String videoID=trailerUrl.substring(trailerUrl.indexOf("watch?v=")+8);
            Log.d(TAG,videoID);
            thumbnailLoader.setVideo(videoID);
        }
    }

    public void onInitializationFailure(YouTubeThumbnailView thumbnailView, YouTubeInitializationResult error) {
        Log.w(TAG,"Couldn't initialize youtube thumbnail view, error : " + error.toString()) ;
    }
}
