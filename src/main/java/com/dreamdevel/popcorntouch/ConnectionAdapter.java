package com.dreamdevel.popcorntouch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ConnectionAdapter extends BaseAdapter {
    private Context context;
    ArrayList<Connection> mConnections;

    public ConnectionAdapter(Context context, ArrayList<Connection> connections) {
        this.context = context;
        mConnections = connections;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listView;

        if (convertView == null) {
            //gridView = new View(context);
            listView = inflater.inflate(R.layout.connection, null);
        } else {
            listView = (View) convertView;
        }

        TextView nameTextView = (TextView) listView.findViewById(R.id.nameTextView);
        nameTextView.setText(mConnections.get(position).getName());


        final ImageButton menuButton = (ImageButton) listView.findViewById(R.id.menuButton);
        menuButton.setFocusable(false);
        menuButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                final ConnectionsActivity connectionsActivity = (ConnectionsActivity) context;
                PopupMenu popup = new PopupMenu(connectionsActivity, menuButton);
                popup.getMenuInflater()
                        .inflate(R.menu.connection_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Delete")) {
                            connectionsActivity.deleteConnection(position);
                        }
                        return true;
                    }
                });

                popup.show();
            }
        });

        return listView;
    }

    @Override
    public int getCount() {
        return mConnections.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
