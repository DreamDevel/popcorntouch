package com.dreamdevel.popcorntouch;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class SaveFile implements Serializable{
    private static final String TAG = "SaveFile";
    private static final String FILE = "SaveFile";
    private static final long serialVersionUID = 1L;

    private Connection mConnection;
    private ArrayList<Connection> mConnections;

    public SaveFile () {
        mConnections = new ArrayList<>();
    }

    public static boolean exists(Context context) {
        File file = new File(context.getFilesDir(), FILE);
        return file.exists();
    }

    public static SaveFile load(Context context) throws IOException {
        try {
            FileInputStream fis = context.openFileInput(FILE);
            ObjectInputStream ois = new ObjectInputStream(fis);
            SaveFile saveFile = (SaveFile) ois.readObject();
            ois.close();
            fis.close();
            Log.i(TAG, "SaveFile Loaded");
            return saveFile;
        } catch (ClassNotFoundException ex) {
            Log.e(TAG,"Couldn't load SaveFile",ex);
            return null;
        }
    }

    public void save(Context context) throws IOException {
            FileOutputStream fos= context.openFileOutput(FILE, Context.MODE_PRIVATE);
            ObjectOutputStream oos= new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
            fos.close();
            Log.i(TAG,"SaveFile Saved");
    }

    public Connection getConnection() {
        return mConnection;
    }

    public void setConnection(Connection connection) {
        mConnection = connection;
    }

    public void setConnectionAndSave(Connection connection, Context context) throws IOException {
        setConnection(connection);
        save(context);
    }

    public ArrayList<Connection> getConnections() {
        return mConnections;
    }

    public void setConnections(ArrayList<Connection> connections) {
        mConnections = connections;
    }

    public void setConnectionsAndSave(ArrayList<Connection> connections, Context context) throws IOException {
        setConnections(connections);
        save(context);
    }
}
