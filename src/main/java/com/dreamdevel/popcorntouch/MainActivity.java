package com.dreamdevel.popcorntouch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dreamdevel.popcornapi.Item;
import com.dreamdevel.popcornapi.ItemCollection;
import com.dreamdevel.popcornapi.PopcornAPIResult;
import com.dreamdevel.popcornapi.Remote;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PopcornAPIResult {
    private static final String TAG = "MainActivity";

    private RecyclerView mItemsRecyclerView;
    private LinearLayout mConnectionAttemptLinearLayout;
    private TextView mTabTextView;
    private ItemCollection mCurrentItemCollection;
    private String mCurrentTab;
    private LinearLayout mNavHeaderLinearLayout;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private TextView mCurrentConnectionTextView;
    private TextView mCurrentConnectionStatusTextView;

    private Integer mGetCurrentListMethodID;
    private Integer mGetNotificationsMethodID;
    private Integer mGetViewStackMethodID;
    private Integer mPingMethodID;


    public static SaveFile saveFile;

    private void setCurrentTab(String tabName){
        mCurrentTab = tabName;
        mTabTextView.setText(mCurrentTab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,"LifeCycle: OnResume");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"LifeCycle: OnCreate");
        setContentView(R.layout.activity_main);
        connectViewsWithObjects();
        setupViewListeners();
        handleExtras();
        initialize();
    }

    private void connectViewsWithObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavHeaderLinearLayout = (LinearLayout) mNavigationView.getHeaderView(0);
        mConnectionAttemptLinearLayout = (LinearLayout) findViewById(R.id.connectionAttemptLinearLayout);
        mCurrentConnectionTextView = (TextView) mNavigationView.getHeaderView(0)
                .findViewById(R.id.currentConnectionTextView);
        mCurrentConnectionStatusTextView = (TextView) mNavigationView.getHeaderView(0)
                .findViewById(R.id.currentConnectionStatusTextView);

        mTabTextView = (TextView) findViewById(R.id.tabTextView);
        mItemsRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        mItemsRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void setupViewListeners() {
        mNavHeaderLinearLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = ConnectionsActivity.newIntent(MainActivity.this);
                startActivity(intent);
            }
        });
    }

    private void handleExtras() {
        // No extras at the moment
    }

    private void initialize() {
        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.getMenu().getItem(0).setChecked(true);
        mNavigationView.setNavigationItemSelectedListener(this);

        setConnectionAttemptView(true);

        try {
            loadSaveFile();
        } catch (IOException ex) {
            Log.e(TAG,"Couldn't load save file",ex);
            saveFile = new SaveFile();
        }

        Connection connection = saveFile.getConnection();
        if(connection != null) {
            updateCurrentConnection(saveFile.getConnection());

            setupRemote();
            pingPopcorn();
        } else {
            Log.e(TAG,"Connection is NULL");
        }
    }

    private void loadSaveFile() throws IOException {
        if(!saveFile.exists(this)) {
            // TODO Start Setup Activity
            saveFile = new SaveFile();
            saveFile.getConnections().add(new Connection("Office-George","192.168.1.2",
                    "8008","popcorn","popcorn"));
            saveFile.getConnections().add(new Connection("LivingRoom-Dinima","192.168.1.5",
                    "8008","popcorn","popcorn"));

            saveFile.setConnection(saveFile.getConnections().get(0));
            saveFile.save(this);
        } else {
            saveFile = SaveFile.load(this);
        }
    }

    private void updateCurrentConnection(Connection connection) {
        mCurrentConnectionTextView.setText(connection.getName());
    }

    private void updateCurrentConnectionStatus(boolean connected) {
        if(connected) {
            mCurrentConnectionStatusTextView.setText("Connection Established");
        } else {
            mCurrentConnectionStatusTextView.setText("Not Connected");
        }

    }

    private void shedulePingPopcorn(int ms) {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() { pingPopcorn(); }
                },
                ms);
    }

    private void pingPopcorn() {
        mPingMethodID = new Remote().ping(this);
    }

    public void onPopcornAPIResult(JSONObject result,Integer methodID) {
        if (result != null)
            Log.d(TAG,"OnPopcornAPIResult: " + result.toString());

        //TODO check for error result before parse
        try {
            if (methodID == mGetCurrentListMethodID) {
                handleGetCurrentListMethodResult(result);
            } else if (methodID == mGetNotificationsMethodID) {
                handleGetNotificationsMethodResult(result);
            } else if (methodID == mGetViewStackMethodID) {
                handleGetViewStackMethodResult(result);
            } else if (methodID == mPingMethodID) {
                handlePingMethodResult(result);
            }
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't parse RPC2 result",ex);
        }
    }

    private void handleGetCurrentListMethodResult(JSONObject result) throws Exception {
        Log.d(TAG,"Handling GetCurrentList Result");
        JSONObject resultOfResult = result.getJSONObject("result");
        mCurrentItemCollection = new ItemCollection(resultOfResult);
        updateGridView();
    }

    private void handleGetNotificationsMethodResult(JSONObject result) throws Exception {
        Log.d(TAG,"Handling GetNotifications Result");
    }

    private void handleGetViewStackMethodResult(JSONObject result) throws Exception {
        Log.d(TAG,"Handling GetViewStack Result");
        JSONObject resultOfResult = result.getJSONObject("result");
        JSONArray viewStack = resultOfResult.getJSONArray("viewstack");

        if(viewStack.length() > 1) {
            // We are not in the movies list
            String viewStackChild = viewStack.getString(1);
            if (viewStackChild.equals("movie-detail") || viewStackChild.equals("app-overlay")) {
                navigateToMovieDetail();
            } else {
                // TODO implement navigation of viewstack
                // Maybe view is about or settings ?
            }
        } else {
            // We are in the movies list
            setCurrentTab("Movies");
        } // TODO check other possible outcomes
    }

    private void handlePingMethodResult(JSONObject result) throws Exception {
        // If callback is been called means that ping was successful
        if(result != null) {
            Log.d(TAG, "Handle Ping Result");
            setConnectionAttemptView(false);
            updateCurrentConnectionStatus(true);
            loadInitialData();
        } else {
            updateCurrentConnectionStatus(false);
            shedulePingPopcorn(1000);
        }

    }

    private void setupRemote() {
        Connection connection = saveFile.getConnection();
        Remote.setup("http://" + connection.getIP(),connection.getPort(),
                connection.getUsername(),connection.getPassword());
    }

    private void loadInitialData() {
        Log.d(TAG,"executing remote methods");
        mGetViewStackMethodID = new Remote().getViewStack(MainActivity.this);
        mGetCurrentListMethodID = new Remote().getCurrentList(1,this);
    }

    private void navigateToMovieDetail() {
        Intent intent = MovieActivity.newMovieIntent(MainActivity.this);
        startActivity(intent);
    }

    private void updateGridView() {
        mItemsRecyclerView.setAdapter(null);
        ItemAdapter itemAdapter = new ItemAdapter(this,mCurrentItemCollection);
        itemAdapter.setClickListener(new ItemAdapter.ItemClickListener(){
            @Override
            public void onItemClick(View view, int position) {
                new Remote().setSelection(position);
                new Remote().enter();
                Item item = mCurrentItemCollection.getList().get(position);
                Intent intent = MovieActivity.newMovieIntent(MainActivity.this,item);
                startActivity(intent);
            }
        });
        mItemsRecyclerView.setAdapter(itemAdapter);
    }

    private void setConnectionAttemptView(boolean visible) {
        if(visible) {
            mConnectionAttemptLinearLayout.setVisibility(View.VISIBLE);
            mItemsRecyclerView.setVisibility(View.GONE);
        } else {
            mConnectionAttemptLinearLayout.setVisibility(View.GONE);
            mItemsRecyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu); TODO create a menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_movies) {
            new Remote().showMovies();
            setCurrentTab("Movies");
            mGetCurrentListMethodID = new Remote().getCurrentList(1,this);
        } else if (id == R.id.nav_series) {
        } else if (id == R.id.nav_anime) {
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
