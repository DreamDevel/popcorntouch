package com.dreamdevel.popcorntouch;

import android.support.v4.media.RatingCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.NotificationCompat;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.session.MediaSessionManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.MediaMetadataCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.dreamdevel.popcornapi.Item;
import com.dreamdevel.popcornapi.PopcornAPIResult;
import com.dreamdevel.popcornapi.Remote;
import org.json.JSONObject;
import java.util.Timer;
import java.util.TimerTask;
import android.os.Binder;

import static android.support.v4.media.MediaMetadataCompat.METADATA_KEY_TITLE;

//@TargetApi(21)
public class MediaPlayerService extends Service implements PopcornAPIResult {
    private static final String TAG = "MediaPlayerService";
    public static final String EXTRA_MOVIE_OBJECT = "com.dreamdevel.popcorntouch.movieObject";
    public static final String EXTRA_TRAILER = "com.dreamdevel.popcorntouch.trailer";
    public static final String MEDIA_SESSION_ID = BuildConfig.APPLICATION_ID + "." + TAG;
    public static final Integer NOTIFICATION_ID = 1;

    public static final String ACTION_PLAY = BuildConfig.APPLICATION_ID + "." + TAG + "ACTION_PLAY";
    public static final String ACTION_PAUSE = BuildConfig.APPLICATION_ID + "." + TAG + "ACTION_PAUSE";
    public static final String ACTION_FAST_FORWARD = BuildConfig.APPLICATION_ID + "." + TAG + "ACTION_FAST_FORWARD";
    public static final String ACTION_REWIND = BuildConfig.APPLICATION_ID + "." + TAG + "ACTION_REWIND";
    public static final String ACTION_STOP = BuildConfig.APPLICATION_ID + "." + TAG + "ACTION_STOP";
    private Double mDuration = 0.0;
    private Double mCurrentSeek = 0.0;
    private String mDownloadSpeed = "n/a";
    private String mQuality = "";

    interface MediaPlayerServiceListener {
        void onPlayStatusChange(boolean isPlaying);
        void onDurationChange(Double duration);
        void onCurrentSeekChange(Double currentTime);
        void onDownloadSpeedChange(String downloadSpeed);
        void onQualityChanged(String quality);
    }

    private MediaPlayerServiceListener mListener;
    private LocalBinder mBinder = new LocalBinder();

    private boolean mIsPopcornPlaying = false;
    private boolean mIsMediaPlayerStatusPlaying = false;
    private boolean mIsPlayerStatusInitialized = false;
    private boolean mServiceInitialized = false;

    private Integer mGetPlayingMethodID;
    private Timer mInfoRequestTimer;
    private MediaSessionCompat mMediaSession;
    private MediaControllerCompat mMediaController;
    private MediaMetadataCompat mMediaMetaData;

    private NotificationCompat.Builder mBuilder;
    private Bitmap coverImage;
    private Item mMovieItem;
    private boolean mIsPlayingTrailer;

    class LocalBinder extends Binder {
        void setMediaPlayerServiceListener (MediaPlayerServiceListener listener) {
            mListener = listener;
        }

        void play() {
            MediaPlayerService.this.onPlay();
        }

        void pause() {
            MediaPlayerService.this.onPause();
        }

        void seek(int seekChange) {
            MediaPlayerService.this.onSeek(seekChange);
        }
    }

    public static Intent newIntent(Context packageContext) {
        return new Intent(packageContext,MediaPlayerService.class);
    }

    public static Intent newIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext,MediaPlayerService.class);
        intent.putExtra(EXTRA_MOVIE_OBJECT,item);
        return intent;
    }

    public static Intent newTrailerIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext,MediaPlayerService.class);
        intent.putExtra(EXTRA_MOVIE_OBJECT,item);
        intent.putExtra(EXTRA_TRAILER,true);
        return intent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG,"onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG,"onStartCommand");
        if (intent != null) {
            handleExtras(intent.getExtras());
        }

        if(!mServiceInitialized) {
            initialize();
        }

        handleIntent( intent );
        return super.onStartCommand(intent, flags, startId);
    }

    private void handleExtras(Bundle extras) {
        if (extras != null && extras.containsKey(EXTRA_MOVIE_OBJECT) && !mServiceInitialized) {
            Log.d(TAG,"Handling Extras");
            mMovieItem = (Item) extras.getSerializable(EXTRA_MOVIE_OBJECT);
            mIsPlayingTrailer = extras.containsKey(EXTRA_TRAILER);

            mMediaMetaData = createMediaMetadataForMovieItem();
        }
    }

    private MediaMetadataCompat createMediaMetadataForMovieItem() {
        return new MediaMetadataCompat.Builder()
                .putString(METADATA_KEY_TITLE, mMovieItem.getTitle()).build();
    }

    private void loadCover(String coverArtURL) {
        Glide.with(this).load(coverArtURL).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                mMediaMetaData = new MediaMetadataCompat.Builder(mMediaMetaData)
                        .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, resource)
                        .putBitmap(MediaMetadataCompat.METADATA_KEY_DISPLAY_ICON, resource)
                        .build();
                mMediaSession.setMetadata(mMediaMetaData);
                coverImage = resource;

                if(mBuilder != null) {
                    mBuilder.setLargeIcon(resource);

                    NotificationManager notificationManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
                    notificationManager.notify( 1, mBuilder.build() );
                }
                Log.d(TAG,"Loaded Cover MetaData");
            }
        });
    }

    private void handleIntent( Intent intent ) {
        if( intent == null || intent.getAction() == null )
            return;
        Log.d(TAG,"Handling Intent");
        String action = intent.getAction();

        if( action.equalsIgnoreCase(ACTION_PLAY ) ) {
            setNotificationPlayingStatus(true);
            onPlay();
        } else if( action.equalsIgnoreCase(ACTION_PAUSE ) ) {
            setNotificationPlayingStatus(false);
            onPause();
        }
    }

    private void initialize () {
        initializeMediaSessions();
        initializeNotification();
        startGettingPlayerInfo();
        mServiceInitialized = true;
    }
    private void initializeMediaSessions() {
        Log.d(TAG,"Initializing Media Session");
        mMediaSession = new MediaSessionCompat(getApplicationContext(), MEDIA_SESSION_ID);
        mMediaSession.setActive(true);
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setPlaybackState(new PlaybackStateCompat.Builder().setState(PlaybackStateCompat.STATE_PLAYING,0,0).build());

        if (mMediaMetaData != null) {
            mMediaSession.setMetadata(mMediaMetaData);
        }

        loadCover(mMovieItem.getCover());

        try {
            mMediaController = new MediaControllerCompat(getApplicationContext(), mMediaSession.getSessionToken());
        } catch (Exception ex) {
            Log.e(TAG,"Couldn't create MediaController",ex);
        }

        mMediaSession.setCallback(new MediaSessionCompat.Callback() {
            @Override
            public void onPlay() {
                super.onPlay();
                MediaPlayerService.this.onPlay();
            }

            @Override
            public void onPause() {
                super.onPause();
                MediaPlayerService.this.onPause();
            }

            @Override
            public void onSkipToNext() {
                super.onSkipToNext();
            }

            @Override
            public void onSkipToPrevious() {
                super.onSkipToPrevious();
            }

            @Override
            public void onFastForward() {
                super.onFastForward();
            }

            @Override
            public void onRewind() {
                super.onRewind();
            }

            @Override
            public void onStop() {
                super.onStop();
            }

            @Override
            public void onSeekTo(long pos) {
                super.onSeekTo(pos);
            }

            @Override
            public void onSetRating(RatingCompat rating) {
                super.onSetRating(rating);
            }
        });

    }

    private void initializeNotification () {
        Log.d(TAG,"Initializing Notification");
        NotificationCompat.MediaStyle style = new NotificationCompat.MediaStyle();
        style.setShowActionsInCompactView(0);
        style.setMediaSession( mMediaSession.getSessionToken() );

        final Intent notificationIntent = new Intent(getApplicationContext(), ControlActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingContentIntent = PendingIntent.getActivity(getApplicationContext(),0,
                notificationIntent,0);

        mBuilder = new NotificationCompat.Builder( this );
        mBuilder.setSmallIcon(R.drawable.ic_movie)
                .setContentTitle(mMovieItem.getTitle())
                .setContentText(mIsPlayingTrailer ? "Movie Trailer" : "Movie")
                .setAutoCancel(false)
                .setOngoing(true)
                .setContentIntent(pendingContentIntent)
                .setStyle(style);

        if (coverImage != null)
            mBuilder.setLargeIcon(coverImage);

        mBuilder.addAction( generateAction( android.R.drawable.ic_media_rew, "Rewind",ACTION_REWIND ) );

        NotificationManager notificationManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
        notificationManager.notify(NOTIFICATION_ID , mBuilder.build());
    }

    private NotificationCompat.Action generateAction( int icon, String title, String intentAction ) {
        Intent intent = new Intent( getApplicationContext(), MediaPlayerService.class );
        intent.setAction( intentAction );
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new NotificationCompat.Action.Builder( icon, title, pendingIntent ).build();

    }

    private void startGettingPlayerInfo() {
        Log.d(TAG,"Started Getting Player Info");
        mInfoRequestTimer = new Timer();
        mInfoRequestTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                mGetPlayingMethodID = new Remote().getPlaying(MediaPlayerService.this);
            }
        },0,500);
    }

    private void stopGettingPlayerInfo() {
        Log.d(TAG,"Stopped Getting Player Info");
        if(mInfoRequestTimer != null)
            mInfoRequestTimer.cancel();
    }

    public void onPopcornAPIResult(JSONObject result, Integer methodID) {
        try {
            if (methodID.intValue() == mGetPlayingMethodID.intValue()) {
                handleGetPlayingMethodResult(result);
            }
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't parse result",ex);
        }
    }

    private void  handleGetPlayingMethodResult(JSONObject result) throws Exception {
        JSONObject resultOfResult = result.getJSONObject("result");
        mIsPopcornPlaying = resultOfResult.getBoolean("playing");

        if(!mIsPlayerStatusInitialized) {
            setPlayingStatus(mIsPopcornPlaying);
            mIsPlayerStatusInitialized = true;
        } else if(mIsPopcornPlaying != mIsMediaPlayerStatusPlaying) {
            setPlayingStatus(mIsPopcornPlaying);
        }

        Double duration = resultOfResult.has("duration") ? resultOfResult.getDouble("duration") : 0;
        Double currentSeek = resultOfResult.has("currentTime") ? resultOfResult.getDouble("currentTime") : 0;

        if (duration.doubleValue() != mDuration.doubleValue()) {
            mDuration = duration;
            mListener.onDurationChange(duration);
        }

        if (currentSeek.doubleValue() != mCurrentSeek.doubleValue()) {
            mCurrentSeek = currentSeek;
            mListener.onCurrentSeekChange(currentSeek);
        }

        String downloadSpeed = resultOfResult.has("downloadSpeed") ?
                resultOfResult.getString("downloadSpeed") : "n/a";

        if (!downloadSpeed.equals(mDownloadSpeed)) {
            mDownloadSpeed = downloadSpeed;
            mListener.onDownloadSpeedChange(downloadSpeed);
        }

        String quality = resultOfResult.has("quality") ?
                resultOfResult.getString("quality") : null;

        if (quality != null && !quality.equals(mQuality)) {
            mQuality = quality;
            mListener.onQualityChanged(quality);
        }

    }

    public void setPlayingStatus (boolean isPlaying) {
        mIsMediaPlayerStatusPlaying = isPlaying;

        if (mListener != null) {
            mListener.onPlayStatusChange(isPlaying);
        }

        setNotificationPlayingStatus(isPlaying);
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"onBind");
        return mBinder;
    }

    private void clearNotification () {
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    private void setNotificationPlayingStatus(boolean isPlaying) {
        mBuilder.mActions.clear();
        Log.d(TAG,"Setting Notification Status To: " + (isPlaying ? "Playing" : "Not Playing"));
        if(isPlaying) {
            mBuilder.addAction( generateAction( R.drawable.ic_pause_item, "Pause", ACTION_PAUSE ) );
        } else {
            mBuilder.addAction( generateAction(R.drawable.ic_play_item, "Play", ACTION_PLAY) );
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
        notificationManager.notify(NOTIFICATION_ID , mBuilder.build());
    }

    private void onPlay() {
        if (!mIsMediaPlayerStatusPlaying) {
            new Remote().togglePlaying();
        }
    }

    private void onPause() {
        if(mIsMediaPlayerStatusPlaying) {
            new Remote().togglePlaying();
        }
    }

    private void onSeek(int seekChange) {
        new Remote().seek(seekChange - mCurrentSeek.intValue());
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mMediaSession.release();
        Log.d(TAG,"onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopGettingPlayerInfo();
        clearNotification();
        Log.d(TAG,"onDestroy");

    }
}