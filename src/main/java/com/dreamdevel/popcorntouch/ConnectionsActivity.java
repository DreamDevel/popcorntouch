package com.dreamdevel.popcorntouch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class ConnectionsActivity extends AppCompatActivity {
    private static final String TAG = "ConnectionsActivity";
    public static ArrayList<Connection> connections;

    private ListView mConnectionsListView;
    private FloatingActionButton mAddFloatingActionButton;

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext,ConnectionsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);

        String[] my_array = {"GEORGE-LIVINGROOM","OFFICE-MAC"};

        ListView my_listview = (ListView)findViewById(R.id.connectionsListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, my_array);
        my_listview.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Connections");

        mAddFloatingActionButton = (FloatingActionButton) findViewById(R.id.addActionButton);
        mAddFloatingActionButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                IntentIntegrator intentIntegrator =  new IntentIntegrator(ConnectionsActivity.this);
                intentIntegrator.setOrientationLocked(false);;
                intentIntegrator.initiateScan();
            }
        });

        mConnectionsListView = (ListView) findViewById(R.id.connectionsListView);
        loadConnections();

        mConnectionsListView = (ListView) findViewById(R.id.connectionsListView);

        mConnectionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Connection selectedConnection = MainActivity.saveFile.getConnections().get(position);
                Log.d(TAG,"Connection : " + selectedConnection.getName() + " with index: " +
                        Integer.toString(position) + " selected");

                try {
                    MainActivity.saveFile.setConnectionAndSave(selectedConnection,getApplicationContext());
                } catch (IOException ex) {
                    Log.e(TAG,"Couldn't save current connection",ex);
                }

            }
        });
    }

    public void loadConnections () {
        ArrayList<Connection> connections = MainActivity.saveFile.getConnections();
        ConnectionAdapter connectionAdapter = new ConnectionAdapter(this,connections);
        mConnectionsListView.setAdapter(connectionAdapter);
        mConnectionsListView.invalidateViews();
    }

    public void editConnection (int index) {

    }

    public void deleteConnection (int index) {

        MainActivity.saveFile.getConnections().remove(index);
        Log.d(TAG,"Removed Connection With Index " + Integer.toString(index));
        loadConnections();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d(TAG,"Scan Cancelled");
                //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                try {
                    Log.d(TAG,result.getContents());
                    JSONObject resultConn = new JSONObject(result.getContents());
                    Connection newConnection = new Connection(resultConn);
                    showConnectionNameDialog(newConnection);
                } catch (Exception ex) {
                    Log.w(TAG,"Couldn't read scanned code",ex);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    // TODO fix Dialog on orientation change
    // TODO make dialog better
    private void showConnectionNameDialog(final Connection connection) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Connection Name");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String inputName = input.getText().toString();
                connection.setName(inputName);
                ArrayList<Connection> connections = MainActivity.saveFile.getConnections();
                connections.add(connection);
                try {
                    MainActivity.saveFile.setConnectionsAndSave(connections,getApplicationContext());
                } catch (IOException ex) {
                    Log.e(TAG,"Couldn't save new connections",ex);
                }

                loadConnections();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
