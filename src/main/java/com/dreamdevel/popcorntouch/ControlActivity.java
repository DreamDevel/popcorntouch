package com.dreamdevel.popcorntouch;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dreamdevel.popcornapi.Item;
import com.dreamdevel.popcornapi.PopcornAPIResult;
import com.dreamdevel.popcornapi.Remote;
import com.dreamdevel.popcornapi.TorrentInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import android.content.ServiceConnection;

import static android.R.attr.duration;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class ControlActivity extends AppCompatActivity implements PopcornAPIResult,ServiceConnection,MediaPlayerService.MediaPlayerServiceListener {
    private static final String TAG = "ControlActivity";
    public static final String EXTRA_MOVIE_OBJECT = "com.dreamdevel.popcorntouch.movieObject";
    public static final String EXTRA_TRAILER = "com.dreamdevel.popcorntouch.trailer";

    private MediaSession mediaSession;

    private Item mMovieItem;
    private TextView mTitleTextView;
    private ImageView mCoverImageView;
    private TextView mConnectionsTextView;
    private TextView mSpeedTextView;
    private TextView mDownloadedTextView;
    private Timer mInfoRequestTimer;
    private TextView mRequiredTextView;
    private ImageButton mPlayPauseImageButton;
    private TextView mCurrentTimeTextView;
    private TextView mDurationTextView;
    private SeekBar mSeekBar;
    private ImageButton mBackImageButton;
    private ProgressBar mProgressBar;
    private LinearLayout mSeekControlsLinearLayout;
    private LinearLayout mMainControlsLinearLayout;
    private TextView mMovieTrailerLableTextView;
    private TableLayout mInfoTableLayout;


    private boolean mIsLoading = false;
    private boolean mIsPlaying = true;
    private boolean mJustToggledPlaying = false;
    private Integer mBackupSeekBarProgress;
    private Integer mUserChangeSeekBarProgress;
    private boolean isSeekBarUsed;
    private boolean mTrailerMode = false;
    private boolean mMediaPlayerServiceStarted = false;

    private String mQuality;
    private Integer mGetPlayingMethodID;
    private Integer mGetLoadingMethodID;
    private Integer mGetViewStackMethodID;
    private Integer mGetSubtitlesMethodID;

    private MediaPlayerService.LocalBinder mMediaPlayerServiceIBinder;
    private Double mDuration = 0.0;

    public static Intent newIntent(Context packageContext) {
        Intent intent = new Intent(packageContext,ControlActivity.class);
        return intent;
    }

    public static Intent newControlIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext,ControlActivity.class);
        intent.putExtra(EXTRA_MOVIE_OBJECT,item);
        return intent;
    }

    public static Intent newControlTrailerIntent(Context packageContext, Item item) {
        Intent intent = new Intent(packageContext,ControlActivity.class);
        intent.putExtra(EXTRA_MOVIE_OBJECT,item);
        intent.putExtra(EXTRA_TRAILER,true);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,"onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        connectViewsWithObjects();
        setupViewListeners();
        handleExtras();
        initialize();
    }

    private void connectViewsWithObjects() {
        mTitleTextView = (TextView) findViewById(R.id.titleTextView);
        mCoverImageView = (ImageView) findViewById(R.id.coverImageView);
        mConnectionsTextView = (TextView) findViewById(R.id.connectionTextView);
        mSpeedTextView = (TextView) findViewById(R.id.speedTextView);
        mDownloadedTextView = (TextView) findViewById(R.id.downloadedTextView);
        mPlayPauseImageButton = (ImageButton) findViewById(R.id.playPauseImageButton);
        mRequiredTextView = (TextView) findViewById(R.id.requiredTextView);
        mCurrentTimeTextView = (TextView) findViewById(R.id.currentTimeTextView);
        mDurationTextView = (TextView) findViewById(R.id.durationTextView);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mBackImageButton = (ImageButton) findViewById(R.id.backImageButton);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mSeekControlsLinearLayout = (LinearLayout) findViewById(R.id.seekControlsLinearLayout);
        mMainControlsLinearLayout = (LinearLayout) findViewById(R.id.mainControlsLinearLayout);
        mMovieTrailerLableTextView = (TextView) findViewById(R.id.movieTrailerLabelTextView);
        mInfoTableLayout = (TableLayout) findViewById(R.id.infoTableLayout);
    }


    private void setupViewListeners(){
        mPlayPauseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsPlaying = !mIsPlaying;
                setPlayPauseButtonStatus(mIsPlaying);

                if (mIsPlaying)
                    mMediaPlayerServiceIBinder.play();
                else
                    mMediaPlayerServiceIBinder.pause();
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mMediaPlayerServiceIBinder.seek(mUserChangeSeekBarProgress);
                isSeekBarUsed = false;
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser) {
                    mUserChangeSeekBarProgress = progress;
                    mCurrentTimeTextView.setText(secondsToTimeString(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSeekBarUsed = true;
            }
        });

        mBackImageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void handleExtras() {
        if (getIntent().getExtras() != null) {
            mMovieItem = (Item) getIntent().getExtras().getSerializable(EXTRA_MOVIE_OBJECT);
            if (getIntent().getExtras().containsKey(EXTRA_TRAILER))
                mTrailerMode = true;
        }

    }

    private void initialize() {
        mTitleTextView.setText(mMovieItem.getTitle());
        Glide.with(this).load(mMovieItem.getCover()).into(mCoverImageView);
        isSeekBarUsed = false;
        mBackupSeekBarProgress = mUserChangeSeekBarProgress = 0;

        if(mTrailerMode) {
            updateLoadingView(false);
            updateTrailerView();
            startMediaPlayerService();
        } else {
            updateLoadingView(true);
            getCurrentRemoteView();
        }
    }

    private void updateLoadingView(boolean isLoading) {
        if (isLoading) {
            mSeekControlsLinearLayout.setVisibility(View.GONE);
            mMainControlsLinearLayout.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mSeekControlsLinearLayout.setVisibility(View.VISIBLE);
            mMainControlsLinearLayout.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void updateTrailerView() {
        mMovieTrailerLableTextView.setVisibility(View.VISIBLE);
        mInfoTableLayout.setVisibility(View.GONE);
    }

    private void startGettingLoadingInfo() {
        Log.d(TAG,"Started Getting Loading Info");
        mInfoRequestTimer = new Timer();
        mInfoRequestTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                mGetLoadingMethodID = new Remote().getLoading(ControlActivity.this);
                mIsLoading = true;
            }
        },0,200);
    }

    private void stopGettingLoadingInfo() {
        Log.d(TAG,"Stopped Getting Loading Info");
        if (mInfoRequestTimer != null)
            mInfoRequestTimer.cancel();
        mIsLoading = false;
    }

    private void getCurrentRemoteView() {
        mGetViewStackMethodID = new Remote().getViewStack(this);
    }


    private void startMediaPlayerService () {
        Intent intent;
        if(mTrailerMode) {
            intent = new MediaPlayerService().newTrailerIntent(getApplicationContext(),mMovieItem);
        } else {
            intent = new MediaPlayerService().newIntent(getApplicationContext(),mMovieItem);
        }
        startService(intent);
        bindService(MediaPlayerService.newIntent(this), this, Context.BIND_AUTO_CREATE);
        mMediaPlayerServiceStarted = true;
    }

    private void stopMediaPlayerService () {
        if(mMediaPlayerServiceStarted) {
            unbindService(this);
            stopService(new MediaPlayerService().newIntent(getApplicationContext()));
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG,"onResume");
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG,"onPause");
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopMediaPlayerService();
        if (mIsLoading)
            stopGettingLoadingInfo();

    }


    @Override
    public void onBackPressed() {
        showStopStreamingDialog();
    }

    private void showStopStreamingDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Leaving will stop streaming")
                .setMessage("Are you sure you want to stop streaming ?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == BUTTON_POSITIVE) {
                            new Remote().back();
                            finish();
                        }
                    }
                }).create().show();
    }


    public void onPopcornAPIResult(JSONObject result, Integer methodID) {
        //TODO add loading too "getloading"
        //TODO check for error result before parse
        try {
            if (methodID == mGetLoadingMethodID) {
                handleGetLoadingMethodResult(result);
            } else if (methodID == mGetViewStackMethodID) {
                handleGetViewStackMethodResult(result);
            } else if (methodID == mGetSubtitlesMethodID) {
                handleGetSubtitlesMethodResult(result);
            }
        } catch (Exception ex) {
            Log.w(TAG,"Couldn't parse result",ex);
        }
    }

    private void handleGetLoadingMethodResult(JSONObject result) throws  Exception {
        // TODO fix popcorntime bug of undefined
        if(result != null) {
            Log.d(TAG,result.toString());
            JSONObject resultOfResult = result.getJSONObject("result");
            boolean loading = resultOfResult.getBoolean("loading");
            if (loading) {
                updateLoadingInfo(resultOfResult);
            }
        }

    }

    private void handleGetViewStackMethodResult(JSONObject result) throws Exception{
        JSONObject resultOfResult = result.getJSONObject("result");
        JSONArray viewStack = resultOfResult.getJSONArray("viewstack");
        Log.d(TAG,"GetViewStack: " + viewStack.toString());
        if ((viewStack.length() == 2 && viewStack.getString(1).equals("app-overlay"))
                || (viewStack.length() == 3 && viewStack.getString(2).equals("notificationWrapper"))) {
            if (!mIsLoading) {
                startGettingLoadingInfo();
            }

            getCurrentRemoteView();
        } else if (viewStack.length() == 2 && viewStack.getString(1).equals("movie-detail")) {
            finish();
        } else if (viewStack.length() > 2 && viewStack.getString(1).equals("app-overlay") &&
                viewStack.getString(viewStack.length()-1).equals("player"))  {
            if (mIsLoading) {
                mIsLoading = false;
                stopGettingLoadingInfo();
            }
            mGetSubtitlesMethodID = new Remote().getSubtitles(this);
            updateLoadingView(mIsLoading);
            startMediaPlayerService();
        }
    }

    private void handleGetSubtitlesMethodResult(JSONObject result) throws Exception{
        Log.d(TAG,result.toString());
        //JSONObject resultOfResult = result.getJSONObject("result");

    }

    private void updateLoadingInfo (JSONObject info) throws Exception {
        mSpeedTextView.setText(info.getString("downloadSpeed"));
        mConnectionsTextView.setText(Integer.toString(info.getInt("activePeers")));
    }

    private String secondsToTimeString(double seconds) {
        Double hours = seconds / 60 / 60;
        Double minutes = (seconds / 60) % 60;
        Double remSeconds = seconds % 60;

        return "0" + Integer.toString(hours.intValue()) + ":" +
                (minutes < 10 ? ("0" + Integer.toString(minutes.intValue())) :
                        Integer.toString(minutes.intValue())) + ":" +
                (remSeconds < 10 ? ("0" + Integer.toString(remSeconds.intValue())) :
                        Integer.toString(remSeconds.intValue()));

    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mMediaPlayerServiceIBinder = (MediaPlayerService.LocalBinder) service;
        mMediaPlayerServiceIBinder.setMediaPlayerServiceListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public void onPlayStatusChange(boolean isPlaying) {
        setPlayPauseButtonStatus(isPlaying);

        // Check if player closed
        if (!isPlaying) {
            getCurrentRemoteView();
        }
    }

    @Override
    public void onCurrentSeekChange(Double currentTime) {
        if(!isSeekBarUsed) {
            mCurrentTimeTextView.setText(secondsToTimeString(currentTime));
            mSeekBar.setProgress(currentTime.intValue());
        }
    }

    @Override
    public void onDurationChange(Double duration) {
        mDuration = duration;
        mDurationTextView.setText(secondsToTimeString(duration));
        mSeekBar.setMax(duration.intValue());

        if (!mTrailerMode)
            setRequiredSpeed(mQuality,mDuration);
    }

    @Override
    public void onDownloadSpeedChange(String downloadSpeed) {
        mSpeedTextView.setText(downloadSpeed);
    }

    @Override
    public void onQualityChanged(String quality) {
        if (!mTrailerMode && quality != null) {
            mQuality = quality;
            setRequiredSpeed(mQuality,mDuration);
        }
    }

    public void setRequiredSpeed(String quality, Double duration) {
        TorrentInfo torrentInfo = mMovieItem.getTorrents().get(quality);
        Double kbps = torrentInfo.getSize() / 1024 / duration;
        Log.d(TAG, "KBPS: " + Double.toString(kbps));
        mRequiredTextView.setText("Required: " + Integer.toString(kbps.intValue()) + " KB/s");
    }

    public void setPlayPauseButtonStatus(boolean isPlaying) {
        if(!isPlaying) {
            mPlayPauseImageButton.setImageResource(R.drawable.ic_play_item);
        } else {
            mPlayPauseImageButton.setImageResource(R.drawable.ic_pause_item);
        }
    }
}
